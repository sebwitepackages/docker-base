#!/bin/bash
set -e

echo "root:$SERVER_PASSWORD" | chpasswd
echo "$SERVER_USERNAME:$SERVER_PASSWORD" | chpasswd
echo "$SERVER_KEY" > /home/$SERVER_USERNAME/.ssh/authorized_keys
chown -R $SERVER_USERNAME:$SERVER_USERNAME /home/$SERVER_USERNAME/.ssh/authorized_keys
chmod 700 /home/$SERVER_USERNAME/.ssh/authorized_keys
