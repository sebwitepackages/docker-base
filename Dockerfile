FROM ubuntu:14.04

MAINTAINER Robin Radic <robin@radic.nl>

# INIT
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y --force-yes software-properties-common python-software-properties
COPY data/root /root
RUN echo "source /.bashrc" >> /etc/profile